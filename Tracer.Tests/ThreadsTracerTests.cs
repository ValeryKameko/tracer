using System.Linq;
using System.Threading;
using NUnit.Framework;
using Tracer.Lib;
using System.Collections.Generic;

namespace Tests
{
    [TestFixture]
    public class ThreadsTracerTests
    {
        private ITracer _tracer;

        [SetUp]
        public void Setup() => _tracer = Tracer.Lib.Tracer.CreateThreadsTracer();

        [Test]
        public void OneMethodTracedThreadsTest()
        {
            TestMethod();

            var result = _tracer.GetTraceResult();
            var tracedThreads = result.TracedThreads;

            Assert.AreEqual(1, tracedThreads.Count);
            Assert.AreEqual(Thread.CurrentThread.ManagedThreadId, tracedThreads.Keys.First());
        }

        [Test]
        public void OneMethodTracedThreadTest()
        {
            TestMethod();

            var result = _tracer.GetTraceResult();
            var tracedThread = result.TracedThreads.Values.First();

            Assert.AreEqual(Thread.CurrentThread.ManagedThreadId, tracedThread.ThreadId);
            Assert.AreEqual(1, tracedThread.TracedMethods.Count());
        }

        [Test]
        public void OneMethodTracedMethodResultTest()
        {
            TestMethod();

            var result = _tracer.GetTraceResult();
            var tracedThread = result.TracedThreads.Values.First();
            var tracedMethod = tracedThread.TracedMethods.First();

            Assert.AreEqual(GetType().FullName, tracedMethod.ClassName);
            Assert.AreEqual("TestMethod", tracedMethod.MethodName);
            Assert.GreaterOrEqual(tracedMethod.Time.Milliseconds, 100);
            Assert.IsEmpty(tracedMethod.TracedInnerMethods);
        }

        private void TestMethod()
        {
            _tracer.StartTrace();
            Thread.Sleep(100);
            _tracer.StopTrace();
        }

        private void RecursionTestMethod(int desiredDepth, int depth = 1)
        {
            _tracer.StartTrace();
            
            if (depth < desiredDepth)
                RecursionTestMethod(desiredDepth, depth + 1);

            _tracer.StopTrace();
        }

        [TestCase(2)]
        [TestCase(10)]
        public void RecursionTest(int recursionDepth)
        {
            RecursionTestMethod(recursionDepth);

            var result = _tracer.GetTraceResult();
            var tracedThread = result.TracedThreads.Values.First();
            var innerMethods = tracedThread.TracedMethods;

            var measuredDepth = 0;
            while (innerMethods.Count() > 0)
            {
                Assert.AreEqual(1, innerMethods.Count());
                var method = innerMethods.First();

                Assert.AreEqual("RecursionTestMethod", method.MethodName);
                Assert.AreEqual(GetType().FullName, method.ClassName);
                innerMethods = method.TracedInnerMethods;

                measuredDepth++;
                Assert.LessOrEqual(measuredDepth, recursionDepth);
            }
            Assert.AreEqual(recursionDepth, measuredDepth);
        }


        [TestCase(2)]
        public void ThreadsTest(int threadCount)
        {
            var threads = new List<Thread>();
            for (int i = 0; i < threadCount; i++)
            {
                threads.Add(new Thread(TestMethod));
            }
            threads.ForEach(thread => thread.Start());
            threads.ForEach(thread => thread.Join());

            var result = _tracer.GetTraceResult();

            Assert.AreEqual(threadCount, result.TracedThreads.Count());
            foreach (var tracedThread in result.TracedThreads)
            {
                Assert.AreEqual(1, tracedThread.Value.TracedMethods.Count());
                var method = tracedThread.Value.TracedMethods.First();

                Assert.AreEqual("TestMethod", method.MethodName);
                Assert.AreEqual(GetType().FullName, method.ClassName);
                Assert.IsEmpty(method.TracedInnerMethods);
            }
        }

        private void CorruptedMethod()
        {
            Thread.Sleep(100);
            _tracer.StopTrace();
        }

        [Test]
        public void StackCorruptionTest()
        {
            Assert.Throws(typeof(TraceException), CorruptedMethod);
        }
    }
}