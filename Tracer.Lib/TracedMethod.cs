﻿using System;
using System.Collections.Generic;

namespace Tracer.Lib
{
    public sealed class TracedMethod
    {
        public string ClassName { get; private set; }
        public string MethodName { get; private set; }
        public TimeSpan Time { get; private set; }
        public IEnumerable<TracedMethod> TracedInnerMethods => _tracedInnerMethods;

        private readonly ICollection<TracedMethod> _tracedInnerMethods;


        internal TracedMethod(string className, string methodName, TimeSpan time, ICollection<TracedMethod> tracedInnerMethods)
        {
            ClassName = className;
            MethodName = methodName;
            Time = time;
            _tracedInnerMethods = tracedInnerMethods;
        }

        internal void AddTracedInnerMethod(TracedMethod method) => _tracedInnerMethods.Add(method);
    }
}