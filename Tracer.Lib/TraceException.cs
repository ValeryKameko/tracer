﻿using System;

namespace Tracer.Lib
{
    public sealed class TraceException : Exception
    {
        public TraceException(string message)
            : base(message)
        {
        }

        public TraceException(string message, Exception innerExcetion)
            : base(message, innerExcetion)
        {
        }
    }
}
