﻿using System.Collections.Generic;

namespace Tracer.Lib
{
    public sealed class TraceResult
    {
        public IReadOnlyDictionary<int, TracedThread> TracedThreads { get; private set; }

        internal TraceResult(IReadOnlyDictionary<int, TracedThread> tracedThreads) => TracedThreads = tracedThreads;
    }
}