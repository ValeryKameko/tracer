﻿using System.IO;

namespace Tracer.Lib
{
    public interface ITraceResultSerializer
    {
        void Serialize(Stream stream, TraceResult traceResult);
    }
}
