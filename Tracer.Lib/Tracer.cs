﻿namespace Tracer.Lib
{
    public static class Tracer
    {
        public static ITracer CreateThreadsTracer() => new Lib.ThreadsTracer.ThreadsTracer();
    }
}
