﻿using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Threading;
using System.Diagnostics;

namespace Tracer.Lib.ThreadsTracer
{
    internal sealed class ThreadsTracer : ITracer
    {
        private readonly ConcurrentDictionary<int, TracingThread> _tracingThreads = new ConcurrentDictionary<int, TracingThread>();

        public ThreadsTracer()
        {

        }

        public TraceResult GetTraceResult()
        {
            var tracedThreads = _tracingThreads.ToDictionary(entry => entry.Key,
                                                             entry => entry.Value.ConvertToTracedThread());
            return new TraceResult(tracedThreads);
        }

        public void StartTrace()
        {
            var tracingThread = GetCurrentTracingThread();
            var method = new StackTrace().GetFrame(1).GetMethod();

            tracingThread.ProcessMethodEnter(method);
        }

        public void StopTrace()
        {
            var tracingThread = GetCurrentTracingThread();
            var method = new StackTrace().GetFrame(1).GetMethod();

            tracingThread.ProcessMethodExit(method);
        }

        private TracingThread GetCurrentTracingThread()
        {
            Func<int, TracingThread> tracingThreadFactory = (threadId) => new TracingThread(threadId);

            var currentThreadId = Thread.CurrentThread.ManagedThreadId;
            return _tracingThreads.GetOrAdd(currentThreadId, tracingThreadFactory);
        }
    }
}
