﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace Tracer.Lib.ThreadsTracer
{
    internal sealed class TracingThread
    {
        private readonly List<TracedMethod> _tracedMethods = new List<TracedMethod>();
        private readonly Stack<TracingMethod> _methodStack = new Stack<TracingMethod>();
        private readonly object _accessLock = new object();

        public int ThreadId { get; private set; }

        public TracingThread(int threadId) => ThreadId = threadId;

        public void ProcessMethodEnter(MethodBase method)
        {
            var tracingMethod = new TracingMethod(method);

            lock (_accessLock)
            {
                _methodStack.Push(tracingMethod);
            }
            tracingMethod.StartTracing();
        }

        public void ProcessMethodExit(MethodBase method)
        {
            lock (_accessLock)
            {
                if (!_methodStack.Any() || !_methodStack.Peek().Method.Equals(method))
                    throw new TraceException("Tracing stack corrupted on method " + method.GetType().FullName + method.Name);
                
                var tracingMethod = _methodStack.Pop();
                tracingMethod.StopTracing();

                var tracedMethod = tracingMethod.ConvertToTracedMethod();
                if (_methodStack.TryPeek(out TracingMethod outerTracingMethod))
                    outerTracingMethod.AddTracedInnerMethod(tracedMethod);
                else
                    _tracedMethods.Add(tracedMethod);
            }
        }

        private TracedMethod UnfoldTracingStack()
        {
            TracedMethod innerTracedMethod = null;
            foreach (var tracingMethod in _methodStack)
            {
                var currentTracedMethod = tracingMethod.ConvertToTracedMethod();
                if (innerTracedMethod != null)
                    currentTracedMethod.AddTracedInnerMethod(innerTracedMethod);
                innerTracedMethod = currentTracedMethod;
            }
            return innerTracedMethod;
        }

        public TracedThread ConvertToTracedThread()
        {
            lock (_accessLock)
            {
                TracedMethod innerTracedMethod = UnfoldTracingStack();

                var tracedMethods = new List<TracedMethod>(_tracedMethods);
                if (innerTracedMethod != null)
                    tracedMethods.Add(innerTracedMethod);

                var time = tracedMethods.Aggregate(TimeSpan.Zero, (accumulator, method) => accumulator + method.Time);
                return new TracedThread(ThreadId, time, tracedMethods);
            }
        }

        public void AddTracedMethod(TracedMethod method) => _tracedMethods.Add(method);
    }
}
