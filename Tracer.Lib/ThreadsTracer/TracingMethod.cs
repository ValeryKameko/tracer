﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace Tracer.Lib.ThreadsTracer
{
    internal sealed class TracingMethod
    {
        public MethodBase Method { get; }

        private readonly Stopwatch _stopwatch = new Stopwatch();
        private readonly List<TracedMethod> _innerTracedMethods = new List<TracedMethod>();

        public TracingMethod(MethodBase method) => Method = method;

        public TracedMethod ConvertToTracedMethod() => new TracedMethod(Method.ReflectedType.FullName,
                                                                        Method.Name,
                                                                        _stopwatch.Elapsed,
                                                                        new List<TracedMethod>(_innerTracedMethods));

        public void StartTracing() => _stopwatch.Start();

        public void StopTracing() => _stopwatch.Stop();

        public void AddTracedInnerMethod(TracedMethod method) => _innerTracedMethods.Add(method);
    }
}
