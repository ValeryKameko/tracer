﻿using System;

namespace Tracer.Lib.TraceResultSerializer
{
    internal static class TraceResultSerializationHelper
    {
        private static string EncodeTimeSpanPart(int value, string postfix)
        {
            return value == 0 ? "" : $"{value}{postfix}";
        }

        public static string SerializeTimeSpan(TimeSpan time)
        {
            string serializedTimeSpan = "";
            serializedTimeSpan += EncodeTimeSpanPart(time.Days, "d");
            serializedTimeSpan += EncodeTimeSpanPart(time.Hours, "h");
            serializedTimeSpan += EncodeTimeSpanPart(time.Minutes, "m");
            serializedTimeSpan += EncodeTimeSpanPart(time.Seconds, "s");
            serializedTimeSpan += EncodeTimeSpanPart(time.Milliseconds, "ms");
            return serializedTimeSpan == "" ? "0ms" : serializedTimeSpan;
        }

    }
}
