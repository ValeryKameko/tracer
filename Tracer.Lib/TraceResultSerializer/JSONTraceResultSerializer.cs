﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Tracer.Lib.TraceResultSerializer
{
    public class JsonTraceResultSerializer : ITraceResultSerializer
    {
        public JsonTraceResultSerializer()
        {
        }

        private JToken SerializeTracedMethods(IEnumerable<TracedMethod> methods)
        {
            return new JArray(from innerMethod in methods
                              select SerializeTracedMethod(innerMethod));
        }

        private JToken SerializeTracedMethod(TracedMethod method)
        {
            var serializedTracedMethod = new JObject
            {
                { "name", method.MethodName },
                { "class", method.ClassName },
                { "time", TraceResultSerializationHelper.SerializeTimeSpan(method.Time) }
            };

            if (method.TracedInnerMethods.Any())
                serializedTracedMethod.Add("methods", SerializeTracedMethods(method.TracedInnerMethods));
            return serializedTracedMethod;
        }

        private JToken SerializeTracedThreads(IEnumerable<TracedThread> threads)
        {
            return new JArray(from thread in threads
                              select SerializeTracedThread(thread));
        }

        private JToken SerializeTracedThread(TracedThread thread)
        {
            return new JObject
            {
                { "id", thread.ThreadId },
                { "time", TraceResultSerializationHelper.SerializeTimeSpan(thread.Time) },
                { "methods", SerializeTracedMethods(thread.TracedMethods) }
            };
        }

        public void Serialize(Stream stream, TraceResult traceResult)
        {
            var orderedTracedThreads = from entry in traceResult.TracedThreads
                                       orderby entry.Key
                                       select entry.Value;

            var serializedTraceResult = new JObject
            {
                { "threads", SerializeTracedThreads(orderedTracedThreads) }
            };

            using (var streamWriter = new StreamWriter(stream, Encoding.ASCII, 4096, true))
            using (var jsonWriter = new JsonTextWriter(streamWriter))
            {
                jsonWriter.StringEscapeHandling = StringEscapeHandling.EscapeNonAscii;
                jsonWriter.Formatting = Formatting.Indented;
                serializedTraceResult.WriteTo(jsonWriter);
            }
        }
    }
}
