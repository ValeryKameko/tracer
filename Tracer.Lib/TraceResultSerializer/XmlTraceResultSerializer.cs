﻿using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Tracer.Lib.TraceResultSerializer
{
    public sealed class XmlTraceResultSerializer : ITraceResultSerializer
    {
        private XElement SerializeTracedMethod(TracedMethod method)
        {
            var serializedTracedMetod = new XElement("method",
                new XAttribute("name", method.MethodName),
                new XAttribute("time", TraceResultSerializationHelper.SerializeTimeSpan(method.Time)),
                new XAttribute("class", method.ClassName));

            if (method.TracedInnerMethods.Any())
                serializedTracedMetod.Add(from innerMethod in method.TracedInnerMethods
                                          select SerializeTracedMethod(innerMethod));
            return serializedTracedMetod;
        }

        private XElement SerializeTracedThread(TracedThread thread)
        {
            return new XElement("thread",
                new XAttribute("id", thread.ThreadId),
                new XAttribute("time", TraceResultSerializationHelper.SerializeTimeSpan(thread.Time)),
                from method in thread.TracedMethods select SerializeTracedMethod(method)
                );
        }

        public void Serialize(Stream stream, TraceResult traceResult)
        {
            var orderedTracedThreads = from entry in traceResult.TracedThreads
                                       orderby entry.Key
                                       select entry.Value;

            var serializedTraceResult = new XDocument(
                new XElement("root", from thread in orderedTracedThreads
                                     select SerializeTracedThread(thread))
                );

            using (var streamWriter = new StreamWriter(stream, Encoding.ASCII, 4096, true))
            using (var xmlWriter = new XmlTextWriter(streamWriter))
            {
                xmlWriter.Formatting = System.Xml.Formatting.Indented;
                serializedTraceResult.WriteTo(xmlWriter);
            }
        }
    }
}
