﻿using System;
using System.Collections.Generic;

namespace Tracer.Lib
{
    public sealed class TracedThread
    {
        public int ThreadId { get; private set; }
        public TimeSpan Time { get; private set; }
        public IEnumerable<TracedMethod> TracedMethods { get; private set; }

        public TracedThread(int threadId, TimeSpan time, IEnumerable<TracedMethod> tracedMethods)
        {
            ThreadId = threadId;
            Time = time;
            TracedMethods = tracedMethods;
        }
    }
}