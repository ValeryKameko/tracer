﻿using System;
using System.IO;

namespace TracerConsoleExample
{
    internal abstract class ITraceResultWriter : IDisposable
    {
        public virtual void Dispose()
        {
        }

        public abstract void Write(MemoryStream resultStream);
    }
}
