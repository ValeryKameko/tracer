﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Tracer.Lib;

namespace TracerConsoleExample.TraceExample
{
    internal sealed class ThreadsExample : ITraceExample
    {
        private readonly ITracer _tracer;

        public ThreadsExample(ITracer tracer)
        {
            _tracer = tracer;
        }

        public void RunExample()
        {
            var threads = new List<Thread>()
            {
                new Thread(() => Func1(100)),
                new Thread(() => Func2(200)),
                new Thread(() => Func3(140))
            };
            threads.ForEach(thread => thread.Start());
            threads.ForEach(thread => thread.Join());
        }

        private void Func1(int millis)
        {
            _tracer.StartTrace();
            Thread.Sleep(millis);
            _tracer.StopTrace();
        }

        private void Func2(int millis)
        {
            _tracer.StartTrace();
            Thread.Sleep(millis);
            _tracer.StopTrace();
        }

        private void Func3(int millis)
        {
            _tracer.StartTrace();
            Thread.Sleep(millis);
            _tracer.StopTrace();
        }
    }
}
