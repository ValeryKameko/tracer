﻿using System.Threading;
using Tracer.Lib;

namespace TracerConsoleExample.TraceExample
{
    internal sealed class FibExample : ITraceExample
    {
        private readonly ITracer _tracer;
        private readonly int _n;

        public FibExample(ITracer tracer, int n)
        {
            this._tracer = tracer;
            this._n = n;
        }

        public void RunExample()
        {
            RecursiveFib(_n);
        }

        private int RecursiveFib(int n)
        {
            _tracer.StartTrace();

            Thread.Sleep(50);
            int result;
            if (n == 0)
                result = 0;
            else if (n == 1)
                result = 1;
            else
                result = RecursiveFib(n - 1) + RecursiveFib(n - 2);
            _tracer.StopTrace();

            return result;
        }
    }
}
