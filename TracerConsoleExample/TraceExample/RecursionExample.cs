﻿using System;
using System.Collections.Generic;
using System.Text;
using Tracer.Lib;

namespace TracerConsoleExample.TraceExample
{
    internal sealed class RecursionExample : ITraceExample
    {
        private readonly ITracer _tracer;
        private readonly int _depth;

        public RecursionExample(ITracer tracer, int depth)
        {
            _tracer = tracer;
            _depth = depth;
        }

        public void RunExample()
        {
            Recursion(_depth);
        }

        private void Recursion(int desiredDepth, int depth = 0)
        {
            _tracer.StartTrace();

            if (depth < desiredDepth)
                Recursion(desiredDepth, depth + 1);

            _tracer.StopTrace();
        }
    }
}
