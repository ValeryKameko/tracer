﻿using System;
using System.Collections.Generic;
using TracerConsoleExample.TraceExample;
using Tracer.Lib;
using Tracer.Lib.TraceResultSerializer;
using System.IO;
using TracerConsoleExample.TraceResultWriter;

namespace TracerConsoleExample
{
    internal sealed class TracerConsoleExample
    {
        private static void Main(string[] args)
        {
            var tracer = Tracer.Lib.Tracer.CreateThreadsTracer();

            var examples = new List<ITraceExample>
            {
                new FibExample(tracer, 3),
                new RecursionExample(tracer, 4),
                new ThreadsExample(tracer)
            };
            examples.ForEach(example => example.RunExample());

            var xmlSerializer = new XmlTraceResultSerializer();
            var jsonSerializer = new JsonTraceResultSerializer();

            var traceResult = tracer.GetTraceResult();

            try
            {
                PrintTraceResult("XML result:", xmlSerializer, traceResult);
                PrintTraceResult("JSON result:", jsonSerializer, traceResult);
                SaveTraceResult("trace_result.xml", xmlSerializer, traceResult);
                SaveTraceResult("trace_result.json", jsonSerializer, traceResult);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private static void SaveTraceResult(string fileName, ITraceResultSerializer serializer, TraceResult result)
        {
            using (var memoryStream = new MemoryStream())
            using (var fileWriter = new FileTraceResultWriter(fileName))
            {
                serializer.Serialize(memoryStream, result);
                fileWriter.Write(memoryStream);
            }
        }

        private static void PrintTraceResult(string description, ITraceResultSerializer serializer, TraceResult result)
        {
            Console.WriteLine(description);
            using (var memoryStream = new MemoryStream())
            {
                serializer.Serialize(memoryStream, result);
                new ConsoleTraceResultWriter().Write(memoryStream);
            }
            Console.WriteLine();
        }
    }
}
