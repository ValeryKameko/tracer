﻿using System.IO;

namespace TracerConsoleExample.TraceResultWriter
{
    internal sealed class FileTraceResultWriter : ITraceResultWriter
    {
        private readonly FileStream _fileStream;

        public FileTraceResultWriter(string filePath)
        {
            _fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write);
        }

        public override void Dispose()
        {
            _fileStream.Flush();
            _fileStream.Close();
            _fileStream.Dispose();
        }

        public override void Write(MemoryStream resultStream)
        {
            resultStream.WriteTo(_fileStream);
        }
    }
}
