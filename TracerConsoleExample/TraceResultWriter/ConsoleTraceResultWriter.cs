﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TracerConsoleExample.TraceResultWriter
{
    internal sealed class ConsoleTraceResultWriter : ITraceResultWriter
    {
        public ConsoleTraceResultWriter()
        {

        }

        public override void Write(MemoryStream resultStream)
        {
            resultStream.WriteTo(Console.OpenStandardOutput());
        }
    }
}
